﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Umbra.Controllers
{
    public class UsersController : Controller
    {
        public IActionResult Index()
        {
            var list = new string[] { "one","two","three"};
            ViewData["Users"] = list;
            return View(@"~/Views/Users/UsersView.cshtml");
        }
    }
}