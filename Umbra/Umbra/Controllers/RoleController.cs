﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Umbra.Controllers
{
    public class RoleController : Controller
    {
        public IActionResult Index()
        {
            var roles = new string[] { "July", "Kim", "Justin" };
            ViewData["Roles"] = roles;
            return View(@"~/Views/Role/RolesView.cshtml");
        }
    }
}