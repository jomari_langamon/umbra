﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DataAccess.Models
{
    public partial class BasicSchoolContext : DbContext
    {
        public virtual DbSet<Classroom> Classroom { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Student> Student { get; set; }
        public virtual DbSet<Users> Users { get; set; }


        public BasicSchoolContext(DbContextOptions<BasicSchoolContext> option):base(option)
        {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Data Source=10.4.37.150\LAPTOP-VPLAC25E\SQLEXPRESS,1433;Initial Catalog=BasicSchool;User ID=sa;Password=jomari");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Classroom>(entity =>
            {
                entity.ToTable("CLASSROOM");

                entity.Property(e => e.Building).HasMaxLength(50);

                entity.Property(e => e.Name).HasColumnType("nchar(10)");

                entity.Property(e => e.Section).HasMaxLength(50);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("ROLE");

                entity.Property(e => e.RoleName).HasMaxLength(50);
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.ToTable("STUDENT");

                entity.Property(e => e.Gender).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.ClassRoom)
                    .WithMany(p => p.Student)
                    .HasForeignKey(d => d.ClassRoomId)
                    .HasConstraintName("FK_STUDENT_CLASSROOM1");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("USERS");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_USERS_ROLE");
            });
        }
    }
}
