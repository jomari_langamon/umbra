﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class Users
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public int? Age { get; set; }
        public string Address { get; set; }
        public int? RoleId { get; set; }

        public Role Role { get; set; }
    }
}
