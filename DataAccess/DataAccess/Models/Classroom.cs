﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class Classroom
    {
        public Classroom()
        {
            Student = new HashSet<Student>();
        }

        public int ClassRoomId { get; set; }
        public string Section { get; set; }
        public string Building { get; set; }
        public string Name { get; set; }
        public int? Capacity { get; set; }

        public ICollection<Student> Student { get; set; }
    }
}
