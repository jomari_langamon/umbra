﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class Student
    {
        public int StudentId { get; set; }
        public int? ClassRoomId { get; set; }
        public int? Age { get; set; }
        public string Gender { get; set; }
        public double? Grade { get; set; }
        public string Name { get; set; }

        public Classroom ClassRoom { get; set; }
    }
}
