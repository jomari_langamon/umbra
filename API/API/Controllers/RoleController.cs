﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/Role")]
    public class RoleController : Controller
    {
        private readonly BasicSchoolContext _context;
        public RoleController(BasicSchoolContext context)
        {
            _context = context;
        }

        // GET: api/Role
        [HttpGet]
        public IEnumerable<Role> Get()
        {
            return _context.Role.ToList();
        }

        // GET api/Role/5
        [HttpGet("{id}", Name = "Get2")]
        public string Get(int id)
        {
            var role = _context.Role.FirstOrDefault(r => r.RoleId == id);
            if (role != null) return role.RoleName;
            return "value";
        }

        // Create
        // POST api/Role
        [HttpPost]
        public IActionResult Post([FromBody]Role value)
        {
            if (value == null)
            {
                return BadRequest();
            }

            _context.Role.Add(value);
            _context.SaveChanges();

            return CreatedAtRoute("Get2", new { id = value.RoleId }, value);
        }

        // Update
        // PUT api/Role/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Role value)
        {
            if (value == null || value.RoleId != id)
            {
                return BadRequest();
            }

            var role = _context.Role.FirstOrDefault(r => r.RoleId == id);

            if (role == null)
            {
                return NotFound();
            }

            role.RoleName = value.RoleName;

            _context.Role.Update(role);
            _context.SaveChanges();

            return new NoContentResult();
        }

        // DELETE api/Role/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var role = _context.Role.FirstOrDefault(r => r.RoleId == id);

            if (role == null)
            {
                return BadRequest();
            }

            _context.Role.Remove(role);
            _context.SaveChanges();

            return new NoContentResult();
        }
    }
}
