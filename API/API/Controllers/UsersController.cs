﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DataAccess.Models;

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : Controller
    {

        private readonly BasicSchoolContext _context;
        public UsersController(BasicSchoolContext context)
        {
            _context = context;
        }
        // GET: api/Users
        [HttpGet]
        public IEnumerable<Users> Get()
        {
            return _context.Users.ToList();
        }

        // GET: api/Users/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            var user = _context.Users.FirstOrDefault(s => s.UserId == id);
            if (user != null) return user.Name;
            return "value";
        }
        
        // POST: api/Users
        [HttpPost]
        public IActionResult Post([FromBody]Users value)
        {
            if (value == null)
            {
                return BadRequest();
            }

            _context.Users.Add(value);
            _context.SaveChanges();

            return CreatedAtRoute("Get", new { id = value.UserId }, value);
        }
        
        // PUT: api/Users/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Users value)
        {
            if (value == null || value.UserId != id)
            {
                return BadRequest();
            }

            var user = _context.Users.FirstOrDefault(s => s.UserId == id);

            if (user == null)
            {
                return NotFound();
            }

            user.UserId = value.UserId;
            user.Name = value.Name;
            user.Age = value.Age;
            user.Address = value.Address;
            user.RoleId = value.RoleId;

            _context.Users.Update(user);
            _context.SaveChanges();

            return new NoContentResult();
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var user = _context.Users.FirstOrDefault(s => s.UserId == id);

            if (user == null)
            {
                return BadRequest();
            }

            _context.Users.Remove(user);
            _context.SaveChanges();

            return new NoContentResult();
        }
    }
}
